<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\ResponseHandler;
use Illuminate\Http\Request;
use File;

class CampaignController extends Controller
{
    public function index()
    {
        $campaigns = Campaign::paginate(10);
        return ResponseHandler::constructResponse(200, "success", $campaigns);
    }

    public function show($id)
    {
        $campaign = Campaign::findOrFail($id);
        return ResponseHandler::constructResponse(200, "success", $campaign);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $campaign = new Campaign();
        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->address = $request->address;
        $campaign->required = $request->required;
        $campaign->collected = $request->collected;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = time() . '.' . $image_extension;
            $image_folder = '/images/campaigns/';
            $image_location = $image_folder . $image_name;

            try{
                $image->move(public_path($image_folder), $image_name);
                $campaign->image = $image_location;

            } catch (\Exception $e) {
                return ResponseHandler::constructResponse(500, $e->getMessage());
            }
        }
        $campaign->save();
        return ResponseHandler::constructResponse(201, "Campaign Berhasil Ditambahkan", $campaign);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
            'required' => 'required',
            'collected' => 'required',
        ]);


        $campaign = Campaign::findOrFail($id);
        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->address = $request->address;
        $campaign->required = $request->required;
        $campaign->collected = $request->collected;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = time() . '.' . $image_extension;
            $image_folder = '/images/campaigns/';

            $subImage = substr($campaign->image, 1);

            File::delete($subImage);

            $image_location = $image_folder . $image_name;

            try{
                $image->move(public_path($image_folder), $image_name);
                $campaign->image = $image_location;

            } catch (\Exception $e) {
                return ResponseHandler::constructResponse(500, $e->getMessage());
            }
        }
        $campaign->save();
        return ResponseHandler::constructResponse(200, "Campaign Berhasil Diupdate", $campaign);
    }

    public function destroy(Request $request, $id)
    {
        $campaign = Campaign::findOrFail($id);
        $subImage = substr($campaign->image, 1);
        File::delete($subImage);
        $campaign->delete();
        return ResponseHandler::constructResponse(200, "Campaign Berhasil Dihapus", $campaign);
    }
}
