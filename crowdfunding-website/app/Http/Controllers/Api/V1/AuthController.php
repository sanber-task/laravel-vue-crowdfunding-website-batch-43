<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\OtpCode;
use App\Models\ResponseHandler;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Mail;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        try{
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request->password),
        ]);
        $data['user'] = $user;

        $token = Auth::login($user);

        $data['token'] = $token;

        // Mail::to($user->email)->send(new \App\Mail\UserRegisterMail($user));

        event(new \App\Events\UserRegistered($user));

        return ResponseHandler::constructResponse(200, "Register Success", $data);
        // return response()->json([
        //     'response_code' => '200',
        //     'response_message' => 'Register Success',
        //     'data' => $user
        // ]);
    } catch (\Exception $e) {
        return ResponseHandler::constructResponse(500, $e->getMessage());
    }
}

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return ResponseHandler::constructResponse(200, "Login Success", $token);

    }

    public function logout(Request $request)
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function getProfile(){
        $data = auth()->user()->load('role');
        return ResponseHandler::constructResponse(200, "Profile Success", $data);
    }

    public function updateProfile(Request $request){

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email',
            'photo_profile' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        $authUser = auth()->user();
        $user = User::where('email',$authUser->email)->first();
        if(auth()->user()->email != $request->email){
            return ResponseHandler::constructResponse(400, "Email not match");
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->hasFile('photo_profile')){
            $image = $request->file('photo_profile');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = time() . '.' . $image_extension;

            $image_folder = '/images/profile/';
            $image_location = $image_folder . $image_name;

            try{
                $image->move(public_path($image_folder), $image_name);
                $user->photo_profile = $image_location;
            } catch (\Exception $e) {
                return ResponseHandler::constructResponse(500, $e->getMessage());
            }
        }
        $user->save();
        return ResponseHandler::constructResponse(200, "Update Profile Success", $user);
    }

    public function updatePassword(Request $request){
        $authUser = auth()->user();
        $user = User::where('email',$authUser->email)->first();
        if(auth()->user()->email != $request->email){
            return ResponseHandler::constructResponse(400, "Email not match");
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return ResponseHandler::constructResponse(200, "Update Password Success", $user);
    }

    public function generateOtp(Request $request){
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email',$request->email)->first();
        if($user){
            $user->generateOtpCode();
            event(new \App\Events\GenerateOtpEvent($user));
            return ResponseHandler::constructResponse(200, "Generate OTP Success", $user);
        } else {
            return ResponseHandler::constructResponse(400, "Email not found");
        }
    }

    public function verificationEmail(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'otp' => 'required',
        ]);

        $otp_code = OtpCode::where('otp',$request->otp)->first();

        if(!$otp_code){
            return ResponseHandler::constructResponse(400, "OTP Code not found");
        }

        $now = Carbon::now();

        if($now > $otp_code->valid_until){
            return ResponseHandler::constructResponse(400, "OTP Code expired");
        } else {
            $user = User::where('email',$request->email)->first();
            $user->email_verified_at = $now;
            $user->save();
            $otp_code->delete();
            return ResponseHandler::constructResponse(200, "Verification Success", $user);
        }
    }
}
