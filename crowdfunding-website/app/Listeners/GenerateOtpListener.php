<?php

namespace App\Listeners;

use App\Events\GenerateOtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;


class GenerateOtpListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\GenerateOtpEvent  $event
     * @return void
     */
    public function handle(GenerateOtpEvent $event)
    {
        Mail::to($event->user->email)->send(new \App\Mail\GenerateOtpMail($event->user));
    }
}
