<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UsesUuid
{

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public function getKeyName()
    {
        return 'id';
    }

    protected static function bootUsesUuid()
    {
        static::creating(function ($model) {
            // $model->id = Str::uuid();
            // if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = \Str::uuid();
            // }
        });
    }
}
