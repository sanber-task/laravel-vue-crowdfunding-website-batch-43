<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResponseHandler extends Model
{
    use HasFactory;

    static function constructResponse($code = 200, $messages = "success", $data = [])
    {
        $response = [
            "meta" => [
                "code" => $code,
                "messages" => $messages
            ],
            "data" => $data
        ];

        return $response;
    }
}
