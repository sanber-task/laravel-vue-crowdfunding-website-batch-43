import{createRouter, createWebHistory} from 'vue-router'
import { useUserStore } from './store/user';

const Home = () => import('@/views/Home.vue');
const About = () => import('@/views/About.vue');
const Verification = () => import('@/views/Verification.vue');
const Campaign = () => import('@/views/Campaign.vue');

const router = createRouter({
    history: createWebHistory(),
    routes : [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/campaign',
            name: 'campaign',
            component: Campaign,
            meta: {
                requiredAdmin: true
            }
        },
        {
            path: '/verification',
            name: 'verification',
            component: Verification,
            meta: {
                requiredVerification: true
            }
        },
        {
            path: '/:catchAll(.*)',
            redirect: '/'
        }
    ],

});
router.beforeEach((to, from) => {
    const auth = useUserStore()
    if(to.meta.requiredVerification){
        if(auth.isVerification){
            alert('Anda tidak memiliki akses ke halaman ini');
            return {
                path: '/'
            }
        }
    }

    if(to.meta.requiredAdmin){
        if(!auth.isAdmin){
            alert('Anda tidak memiliki akses ke halaman ini');
            return {
                path: '/'
            }
        }
    }
});

export default router;