import {defineStore} from 'pinia'

export const useUserStore = defineStore('user', {
    state: () => {
        return {
            token: "",
            user: null,
            isLogin: false,
            isAdmin: null,
            isVerification: null,
        }
    },
    actions: {
        async setToken(token) {
            this.token = token;
            
            const config = {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            };
            try {
                const userData = await axios.get('/api/v1/get-profile', config);
                const userRole = userData.data.data.role;
                this.user = userData.data.data;
                if(userRole.name === 'admin') {
                    this.isAdmin = true;
                } else {
                    this.isAdmin = false;
                }

                if(userData.data.data.email_verified_at != null) {
                    this.isVerification = true;
                } else {
                    this.isVerification = false;
                }
                
            } catch (error) {
                console.log(error);
            }
            this.isLogin = true;
        },

        async removeAuth(){
            this.token = "";
            this.user = null;
            this.isLogin = false;
            this.isAdmin = null;
            this.isVerification = null;
        },

        async setVerification(user, token){
            this.token = token;
            this.user = user;
            this.isLogin = true;
            this.isVerification = false;
            this.isAdmin = false;
        },

        async verificationEmail(){
            this.isVerification = null;
        }
    },
    persist: true
})