<?php

use App\Http\Controllers\Api\V1\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'namespace'=>'App\Http\Controllers\Api\V1'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('register', [AuthController::class,'register']);
        Route::post('login', [AuthController::class,'login']);
        Route::post('generate-otp', [AuthController::class,'generateOtp']);
        Route::post('verification-email', [AuthController::class,'verificationEmail']);
    });

    Route::group(['middleware' => ['auth', 'email_verification']], function () {
        Route::post('logout', [AuthController::class,'logout']);
        Route::post('update-password', [AuthController::class,'updatePassword']);
        Route::post('update-profile', [AuthController::class,'updateProfile']);
    });
    
    Route::group(['middleware' => ['auth', 'isAdmin']], function () {
        Route::apiResource('campaigns', CampaignController::class);
    });
    Route::get('get-profile', [AuthController::class,'getProfile']);
});
