export const UserComponent = {
    template: 
    `<div class="usersList">
        <ul>
            <li v-for="(user , index) in reverseusers" :key="index">
                {{ user.name }} ||
                <button @click="edituser(index)">Edit</button>
                <button @click="deleteuser(index)">Delete</button>
            </li>
        </ul>
    </div>`,
    props: ['reverseusers', 'edituser', 'deleteuser']
}