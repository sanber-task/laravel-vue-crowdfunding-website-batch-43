//SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

daftarHewan.forEach(function(item) {
    console.log(item);
});

// SOAL 2

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)

function introduce(data){
    console.log("Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby + "!")
}

//SOAL 3

function hitung_huruf_vokal(kata) {
    var vokal = ["a", "i", "u", "e", "o"];
    var jumlah = 0;
    for (var i = 0; i < kata.length; i++) {
        if (vokal.indexOf(kata.toLowerCase()[i]) !== -1) {
            jumlah += 1;
        }
    }
    return jumlah;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);

// SOAL 4

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result = [];
    for (var i = 0; i < arrPenumpang.length; i++) {
        var obj = {};
        obj.penumpang = arrPenumpang[i][0];
        obj.naikDari = arrPenumpang[i][1];
        obj.tujuan = arrPenumpang[i][2];
        obj.bayar = (rute.indexOf(obj.tujuan) - rute.indexOf(obj.naikDari)) * 2000;
        result.push(obj);
    }
    return result;
}

var penumpang = ['Dimitri', 'B', 'F'];

var penumpang2 = ['Adit', 'A', 'C'];

console.log(naikAngkot([penumpang, penumpang2]));


// ES6

// SOAL 1

let luasSegitiiga = (alas, tinggi) => alas * tinggi / 2;
let kelilingSegitiga = (alas, tinggi) => alas + tinggi + Math.sqrt(alas * alas + tinggi * tinggi);

console.log(luasSegitiiga(10, 20));
console.log(kelilingSegitiga(10, 20));

// SOAL 2

const newFunction = (firstName, lastName) => { return { firstName, lastName, fullName: () => { console.log(firstName + " " + lastName) } } };
console.log(newFunction("William", "Imoh").fullName());

// SOAL 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby);

// SOAL 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];

console.log(combined);

// SOAL 5

const planet = "earth";
const view = "glass";

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);