<?php 
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi() {
        echo "{$this->nama } sedang {$this->keahlian}";
    }
}

abstract class Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan){
        echo "{$this->nama} sedang diserang {$hewan->nama} <br>";
        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
        echo "Sisa darah {$this->nama} adalah {$this->darah} <br>";
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight {

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan() {
        echo "Nama: {$this->nama} <br>";
        echo "Jumlah Kaki: {$this->jumlahKaki} <br>";
        echo "Keahlian: {$this->keahlian} <br>";
        echo "Attack Power: {$this->attackPower} <br>";
        echo "Defence Power: {$this->defencePower} <br>";
        $this->atraksi();
    }
}

class Harimau extends Fight {

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan() {
        echo "Nama: {$this->nama} <br>";
        echo "Jumlah Kaki: {$this->jumlahKaki} <br>";
        echo "Keahlian: {$this->keahlian} <br>";
        echo "Attack Power: {$this->attackPower} <br>";
        echo "Defence Power: {$this->defencePower} <br>";
        $this->atraksi();
    }
}

$elang = new Elang("Elang");
$harimau = new Harimau("Harimau");

$elang->getInfoHewan();
echo "<hr>";
$harimau->getInfoHewan();
echo "<hr>";
$elang->serang($harimau);
echo "<hr>";
$harimau->serang($elang);
?>

